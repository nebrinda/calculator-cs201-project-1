//modified by nathan brinda
#ifndef VALUE_H
#define VALUE_H

typedef struct Value
{
   int type;
   int ival;
   int precedence;
   double rval;
   char *sval;
   char* varname;
   int vtype;

} Value;


extern int NOTSET;
extern int INTEGER;
extern int REAL;
extern int STRING;
extern int VARIABLE;
extern int OPERATOR;

extern Value *newValueI(int);
extern Value *newValueR(double);
extern Value *newValueS(char *);
extern Value *newValueV(char *);
extern Value *newValueO(char *);
void Fatal(char *fmt, ...);

#define true 1
#define false 0

#endif
