//written by nathan brinda
#include "Node.h"
#include <stdlib.h>

Node* new_node(){
   Node* node = malloc(sizeof(Node));
   node->node_value = NULL;
   node->next = NULL;
   node->prev = NULL; 
   return node;
}