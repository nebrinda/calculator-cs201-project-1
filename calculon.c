//written by nathan brinda
#include "Utils.h"

#include <stdlib.h>

int main(int argc, char** argv){
   BST* variables = initializeBST();
   Queue* postfix = initializeQueue();
   FILE* fp;
   
   switch(argc){
      case 1:
         process_values(stdin,variables,"");
         break;
      case 2:
         if (!strcmp(argv[1], "-v")){
            print_name();
            break;
         }
         else if (!strcmp(argv[1], "-d")){
            postfix = process_values(stdin,variables,"-d");
            print_queue(postfix);
            break;
        }
         else{
            fp = fopen(argv[1], "r");
            if (fp == NULL)
               Fatal("unable to open file");
            process_values(fp, variables,"");
            print_queue(postfix);
            fclose(fp);
            break;
         }
      case 3:
         fp = fopen(argv[2], "r");
         if (fp == NULL)
            Fatal("unable to open file");
         postfix = process_values(fp, variables,"-d");
         print_queue(postfix);
         fclose(fp);
         break;
               
      default:
         Fatal("arguments not recognized");
         break;         
   }  
   
   return 0;
}











