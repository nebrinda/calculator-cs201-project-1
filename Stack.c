//written by nathan brinda

#include "Stack.h"
#include "Value.h"
#include <stdio.h>
#include <stdlib.h>


Stack* initializeStack(){
   Stack* stack = malloc(sizeof(Stack));
   if ((stack = malloc(sizeof(Value))) == 0)
      Fatal("out of memory\n");
   stack->number_of_elements = 0;
   stack->top = NULL;
   return stack;
}

//shoud probably pass the stack by reference.
//need to see if the * there is already doing that.
Value* pop(Stack* my_stack){
   Node* temp = new_node();
   Value* value;

   //stack is empty
   if (my_stack->number_of_elements == 0)
      return NULL;

   //only 1 item in stack, return the only item and free memory
   if (my_stack->number_of_elements == 1){
      my_stack->number_of_elements = 0;
      value = my_stack->top->node_value;
      my_stack->top = NULL;
      free(my_stack->top);
      return value;
   
   }
   //more than 1 value in the stack
   temp = my_stack->top->next;
   my_stack->number_of_elements--;
   value = my_stack->top->node_value;
   my_stack->top = NULL;
   free(my_stack->top);
   my_stack->top = temp;
   temp = NULL;
   free(temp);
   return value;
}


void push(Stack* my_stack, Value* value_to_add){
   Node* node = new_node();
   node->node_value = value_to_add;

   //if it is empty add it to the top
   if (my_stack->number_of_elements == 0){
      my_stack->top = node;
      my_stack->number_of_elements++;
   }
   //not empty, make the new element the top and update ptrs
   else{
      Node* temp = new_node();
      temp = my_stack->top;
      my_stack->top = node;
      my_stack->top->next = temp;
      my_stack->number_of_elements++;
   }
   node = NULL;
   free(node);
}