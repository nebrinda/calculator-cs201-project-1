//written by nathan brinda
#include <stdio.h>
#include <stdlib.h>
#include "Queue.h"
#include "Value.h"

Queue* initializeQueue(){
   Queue* queue = malloc(sizeof(Queue));
   if ((queue = malloc(sizeof(Value))) == 0)
      Fatal("out of memory\n");
   queue->number_of_elements = 0;
   queue->bottom = NULL;
   queue->top = NULL;
   return queue;
}


void enqueue(Queue *my_queue,Value* value){

   Node* node = new_node();
   node->node_value = value;
   node->next = NULL;
   node->prev = NULL;

   //the queue is empty
   if (my_queue->number_of_elements == 0){  
      my_queue->bottom = node;
      my_queue->top = node;
      my_queue->number_of_elements++;
     }
   else{//the queue is not empty
      Node* temp = new_node();
      temp = my_queue->top;
      my_queue->top = node;
      my_queue->top->next = temp;
      my_queue->top->next->prev = my_queue->top;
      my_queue->number_of_elements++;
      temp = NULL;
      free(temp);
   }
   //free allocated memory
   node = NULL;
   free(node);
}

//returns null if the queue is empty
Value* dequeue(Queue *my_queue){

   Node* value = new_node();

   //queue is empty
   if (my_queue->number_of_elements == 0)
      return NULL;

   //queue has exactly 1 item
   if (my_queue->number_of_elements == 1){
      my_queue->number_of_elements = 0;
      value = my_queue->bottom;
      my_queue->bottom = NULL;
      free(my_queue->bottom);
      //free(my_queue->top);
      return value->node_value;
   }
   //queue has more than 1 item
   Node* temp = new_node();
   temp = my_queue->bottom;
   my_queue->bottom = my_queue->bottom->prev;
   my_queue->bottom->next = NULL;
   value = temp;
   temp = NULL;
   free(temp);
   my_queue->number_of_elements--;
   return value->node_value;
}