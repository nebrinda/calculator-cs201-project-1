//written by nathan brinda
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <signal.h>

#include "scanner.h"
#include "Utils.h"
#include "Stack.h"

static Value* handle_single_string(Value* first_value, Value* second_value, Value* op, BST* variables);
static Value* handle_double_strings(Value* first_value, Value* second_value);
static void store_variable(Value* token, BST* variables);
static void switch_arg_type(Value* value1, Value* value2, int type1);
static int type_to_use(Value* v);
static Value* compute_power(Value* value1, Value* value2, int type1, int type2);
static Value* compute_value(Value* first_value, Value* second_value, Value* op, int first_type, int second_type);
static Queue* infix_to_postfix(Queue* infix);

static int isString(char* token);
static int isInt(char* token);
static int isReal(char* token);
static int isVar(char* token);
static void setType(Value* value, Value* first_value, int type_to_set);
static void removePreviousExpression(Queue* expression);
static void addToCorrectQueue(Queue* expression, Queue* values_for_variables, Value* value_object, int after_equals);
static void keepTypesConsistent(BSTNode* BST_var, Value* var);
static Value* processValueAfterSemiColon(Queue* values_for_variables, BST* variables, char* var_before_equals);



//returns a Queue in postfix notation 
Queue* process_values(FILE* fp, BST* variables, char* wantPostfix){
   Value* value_object = newValueI(0);
   Value* calculated_value = newValueI(0);
   Queue* expression = initializeQueue();
   Queue* values_for_variables = initializeQueue();
   char* next_token = "";
   char* previous_token = "";
   char* var_before_equals = "";
   int after_equals = false;
   int counter = 0;
   int isOddExpression = false;

   while (true){
      //string coming in
      if (stringPending(fp)) {
         next_token = readString(fp);
         counter++;
         value_object = newValueS(next_token);
         previous_token = next_token;
         addToCorrectQueue(expression, values_for_variables, value_object, after_equals);
         continue;
      }

      //not a string, just read it like a normal token
      next_token = readToken(fp);
      counter++;
      if (next_token == 0)
         break;
      

      //delete previous expressions
      if (next_token != 0 && previous_token[0] == ';'){
         removePreviousExpression(expression);
         removePreviousExpression(values_for_variables);
      }

      //token is int
      if (isInt(next_token)){
         value_object = newValueI(atoi(next_token));
         addToCorrectQueue(expression, values_for_variables, value_object, after_equals);
      }

      //token is float
      else if (isReal(next_token)){
         value_object = newValueR(atof(next_token));
         addToCorrectQueue(expression, values_for_variables, value_object, after_equals);
      }

      //token is variable
      else if (isVar(next_token)){
         value_object->varname = next_token;
         BSTNode* node = search(variables->top, value_object);
         if (!strcmp(previous_token, "var") || node){
            //if (node != NULL && after_equals || (strcmp(var_before_equals, "") == 0 && !var_has_been_seen))
            if (node != NULL && after_equals)
               addToCorrectQueue(expression, values_for_variables, node->node_value, after_equals);
            else if (strcmp(var_before_equals, "") == 0 && node != NULL){
               addToCorrectQueue(expression, values_for_variables, node->node_value, after_equals);
               isOddExpression = true;
            }
            if (node || after_equals){
               previous_token = next_token;
               continue;
            }
            value_object = newValueV(next_token);
            store_variable(value_object, variables);
         }
         else{
            //var not declared, terminate program
            printf("variable %s was not declared\n", next_token);
            exit(-1);}
      }

      //equals sign reached, set a flag
      else if (next_token[0] == '='){
         if (previous_token[0] != ';') 
            var_before_equals = previous_token;
         after_equals = true;
      }

      //semi-colon hit, process expression
      else if (next_token[0] == ';'){
         if (after_equals == true){
            calculated_value = processValueAfterSemiColon(values_for_variables, variables, var_before_equals);
            var_before_equals = "";
            after_equals = false;
            isOddExpression = false;
            counter = 0;
         }
         else{
            expression = infix_to_postfix(expression); 
            if (strcmp(wantPostfix, "-d") != 0)
               calculated_value = process_postfix_expression(expression, variables);
         }
      }

      //token is operator
      else {
         if (strcmp(next_token, "var")){
            value_object = newValueO(next_token);
            addToCorrectQueue(expression, values_for_variables, value_object, after_equals);
         }
      }
      previous_token = next_token;
   }

   if (strcmp(wantPostfix, "-d") != 0){
      print_calculated_value(calculated_value);
      exit(-1);
   }

   if (expression->number_of_elements == 0 || (isOddExpression != true && values_for_variables->number_of_elements > 0))
      return values_for_variables;
   else
      return expression;
}


//calculates the expression after a semi-colon and returns the value.
static Value* processValueAfterSemiColon(Queue* values_for_variables, BST* variables, char* var_before_equals){
   BSTNode* BST_var = new_BSTnode();
   Value* value_object = newValueI(0);
   Queue* temp_queue = initializeQueue();
   Value* var = newValueV(var_before_equals);

   value_object = newValueV(var_before_equals);
   BST_var = search(variables->top, value_object);
   temp_queue = infix_to_postfix(values_for_variables);
   *values_for_variables = *temp_queue;
   var = process_postfix_expression(temp_queue, variables);
   keepTypesConsistent(BST_var, var);

   return var;

}

//updates the types of the variable after calculating its value
static void keepTypesConsistent(BSTNode* BST_var, Value* var){
   char* save_var_name = BST_var->node_value->varname;

   if (BST_var->node_value->vtype == NOTSET)
      var->vtype = var->type;
   *BST_var->node_value = *var;
   BST_var->node_value->varname = save_var_name;
   BST_var->node_value->type = VARIABLE;
}

//adds the value given to the queue that it is supposed to be in
static void addToCorrectQueue(Queue* expression, Queue* values_for_variables, Value* value_object,int after_equals){
   Value* temp = newValueI(0);
   *temp = *value_object;
   if (after_equals)
      enqueue(values_for_variables, temp);
   else
      enqueue(expression, temp);
}


//takes in a queue and returns a Value* with a single literal in it.
Value* process_postfix_expression(Queue* values_to_process, BST* variables){

   Stack* compute_literal = initializeStack();
   Value* postfix_value = newValueI(0);
   int type1 = 0;
   int type2 = 0;

   while (values_to_process->number_of_elements > 0){
      postfix_value = dequeue(values_to_process);
      if (postfix_value->type == OPERATOR){
         Value* first_value = pop(compute_literal);
         Value* second_value = pop(compute_literal);
         type1 = type_to_use(first_value);
         type2 = type_to_use(second_value);

         if (type1 == STRING && type2 == STRING){
            postfix_value = handle_double_strings(first_value, second_value);
            push(compute_literal, postfix_value);
         }
         else if (type1 == STRING || type2 == STRING){
            postfix_value = handle_single_string(first_value, second_value, postfix_value, variables);
            push(compute_literal, postfix_value);
         }
         else{
            postfix_value = compute_value(first_value, second_value, postfix_value, type1, type2);
            push(compute_literal, postfix_value);
         }
      }
      else
         push(compute_literal, postfix_value);
   }
   if (compute_literal->number_of_elements > 0)
     postfix_value = pop(compute_literal);
   return postfix_value;
}

//prints out a value based on it's type
void print_calculated_value(Value* calculated_value){

   if (calculated_value->type == VARIABLE){
      if (calculated_value->vtype == INTEGER)
         printf("%d", calculated_value->ival);
      else if (calculated_value->vtype == REAL)
         printf("%f", calculated_value->rval);
      else if (calculated_value->vtype == STRING)
         printf("\"%s\"", calculated_value->sval);
      else
         Fatal("cannot print calculated value");
   }
   else {
      if (calculated_value->type == INTEGER)
         printf("%d", calculated_value->ival);
      else if (calculated_value->type == REAL)
         printf("%f", calculated_value->rval);
      else if (calculated_value->type == STRING)
         printf("\"%s\"", calculated_value->sval);
      else
         Fatal("cannot print calculated value");
   }
   printf("\n");
}

//prints the author's name
void print_name(){
   printf("Nathan Brinda\n");
   exit(0);
}

//dequeues all the values off of a given queue, printing them
void print_queue(Queue* queue){
   Value* temp_value;

   while (queue->number_of_elements > 0){
      temp_value = dequeue(queue);
      if (temp_value->type == VARIABLE){
         printf("%s ", temp_value->varname);
      }
      else
         if (temp_value->type == INTEGER)
            printf("%d ", temp_value->ival);
         else if (temp_value->type == REAL)
            printf("%f ", temp_value->rval);
         else{
            if (temp_value->type == OPERATOR)
               printf("%s ", temp_value->sval);
            else 
               printf("\"%s\" ", temp_value->sval);        
         }
            
   }
   printf("\n");
}


//removes the previous expression from the queue
static void removePreviousExpression(Queue* expression){
   while (expression->number_of_elements > 0)
      dequeue(expression);
}

//converts an infix expression to a postfix expression
static Queue* infix_to_postfix(Queue* infix){
   Value* element;
   Queue* tempQueue = initializeQueue();
   *tempQueue = *infix;
   Queue* postfix = initializeQueue();
   Stack* operators = initializeStack();
   int previous_precedence = 0;

   while (tempQueue->number_of_elements > 0){
      element = dequeue(tempQueue);

      if (element->type == OPERATOR){     
         if (previous_precedence < element->precedence || element->sval[0] == '('){
            push(operators, element);
            previous_precedence = element->precedence;
         }
         else if (element->sval[0] == ')'){ //closing parenthesis, pop until you reach an opening parenthesis
            while (operators->number_of_elements > 0){
               element = pop(operators);
               if (element->sval[0] == '(')
                  break;
               enqueue(postfix, element);
            }
            if (operators->number_of_elements > 0){
               element = pop(operators);
               previous_precedence = element->precedence;
               push(operators, element);
            }
         }

         else{ //precedence in wrong order
            Node* temp = new_node();
            while (operators->number_of_elements > 0){
               temp->node_value = pop(operators);
               if (temp->node_value->precedence >= element->precedence)
                  enqueue(postfix, temp->node_value);
               //put value back on, and break
               else{
                  push(operators, temp->node_value);
                  break;
               }
            }
            push(operators, element); ///don't lose operators
            previous_precedence = element->precedence;
         }
      }
      //not an operator
      else
         enqueue(postfix, element);
   }
   while (operators->number_of_elements != 0)
      enqueue(postfix, pop(operators));
   return postfix;
}


//returns true if the string is truly a string
static int isString(char* token){
   if (token[0] == '"')
      return true;
   else
      return false;
}

//returns true if a string is an integer
static int isInt(char* token){
   if (strchr(token, '.'))
      return false;
   if (token[0] == '-'){
      if (isdigit(token[1])){
         return true;
      }
   }
   else if (isdigit(token[0]))
      return true;

   return false;
}

//returns true if a string is a real
static int isReal(char* token){
   if (strchr(token, '.') && !(isString(token)))
      return true;
   else
      return false;
}

//returns true if a string is a variable
static int isVar(char* token){
   if (isalpha(token[0]) && strcmp(token, "var"))
      return true;
   else
      return false;
}

//returns the correct type given a value
static int type_to_use(Value* v){

   if (v->type == VARIABLE)
      return v->vtype;
   else
      return v->type;
}

//raises value2 to the value1 power, i.e. value2 is the base and value1 the power
static Value* compute_power(Value* value1, Value* value2, int type1, int type2){

   if (type1 == INTEGER && type2 == INTEGER)
      return newValueI((int)pow(value2->ival, value1->ival));
   else if (type1 == REAL && type2 == INTEGER)
      return newValueR(pow((double)value2->ival, value1->rval));
   else if (type2 == REAL && type1 == INTEGER)
      return newValueR(pow(value2->rval, (double)value1->ival));
   else
      return newValueR(pow(value2->rval, value1->rval));

}

//sets the type of a given value
static void setType(Value* value, Value* first_value, int type_to_set){
   if (first_value->type == VARIABLE)
      value->vtype = type_to_set;
   else
      value->type = type_to_set;

}


//calculates a value given to values
static Value* compute_value(Value* first_value, Value* second_value, Value* op, int first_type, int second_type){
   Value* temp = newValueI(0);
   *temp = *second_value;

   if (first_type == INTEGER){
      if (second_type == INTEGER){
         setType(temp, first_value, INTEGER);
         if (op->sval[0] == '+')
            temp->ival = second_value->ival + first_value->ival;
         else if (op->sval[0] == '-')
            temp->ival = second_value->ival - first_value->ival;
         else if (op->sval[0] == '*')
            temp->ival = second_value->ival * first_value->ival;
         else if (op->sval[0] == '/')
            temp->ival = second_value->ival / first_value->ival;
         else if (op->sval[0] == '^'){
            temp = compute_power(first_value, second_value, first_type, second_type);
         }
         else
            temp->ival = second_value->ival % first_value->ival;
         return temp;
      }

      else if (second_type == REAL) {
         setType(temp, first_value, REAL);
         if (op->sval[0] == '+')
            temp->rval = second_value->rval + first_value->ival;
         else if (op->sval[0] == '-')
            temp->rval = second_value->rval - first_value->ival;
         else if (op->sval[0] == '*')
            temp->rval = second_value->rval * first_value->ival;
         else if (op->sval[0] == '/')
            temp->rval = second_value->rval / first_value->ival;
         else if (op->sval[0] == '%')
            temp->rval = fmod(second_value->rval, (double)first_value->ival);
         else{
            temp = compute_power(first_value, second_value, first_type, second_type);
         }
      }
      return temp;
   }
   else if (first_type == REAL){
      if (second_type == REAL){
         setType(temp, first_value, REAL);
         if (op->sval[0] == '+')
            temp->rval = second_value->rval + first_value->rval;
         else if (op->sval[0] == '-')
            temp->rval = second_value->rval - first_value->rval;
         else if (op->sval[0] == '*')
            temp->rval = second_value->rval * first_value->rval;
         else if (op->sval[0] == '/')
            temp->rval = second_value->rval / first_value->rval;
         else if (op->sval[0] == '%')
            temp->rval = fmod(second_value->rval, first_value->rval);
         else
            temp = compute_power(first_value, second_value, first_type, second_type);
         return temp;
      }
      else{
         if (second_type == INTEGER){
            setType(temp, first_value, REAL);
            if (op->sval[0] == '+')
               temp->rval = second_value->ival + first_value->rval;
            else if (op->sval[0] == '-')
               temp->rval = second_value->ival - first_value->rval;
            else if (op->sval[0] == '*')
               temp->rval = second_value->ival * first_value->rval;
            else if (op->sval[0] == '/')
               temp->rval = second_value->ival / first_value->rval;
            else if (op->sval[0] == '%')
               temp->rval = fmod((double)second_value->ival, first_value->rval);
            else
               temp = compute_power(first_value, second_value, first_type, second_type);
            return temp;
         }
      }
   }
   return NULL;
}


//determines what inputs to give to compute_value
static Value* handle_single_string(Value* first_value, Value* second_value, Value* op, BST* variables){
   BSTNode* temp = new_BSTnode();
   temp->node_value = newValueI(0);

   int type1 = type_to_use(first_value);
   int type2 = type_to_use(second_value);

   if (type1 == STRING){
      if (type2 == INTEGER){
         first_value->ival = atoi(first_value->sval);
         return compute_value(first_value, second_value, op, INTEGER, INTEGER);
      }

      else if (type2 == REAL){
         if (isInt(first_value->sval)){
            first_value->ival = atoi(first_value->sval);
            return compute_value(first_value, second_value, op, INTEGER, REAL);
         }
         else {
            first_value->rval = atof(first_value->sval);
            return compute_value(first_value, second_value, op, REAL, REAL);
         }
      }

   }
   else{
      Value* temp_first_val = newValueI(0);
      Value* temp_second_val = newValueI(0);
      *temp_first_val = *first_value;
      *temp_second_val = *second_value;

      if (op->sval[0] == '^'){ 
         switch_arg_type(temp_first_val, temp_second_val, type1);
         return handle_single_string(temp_first_val, temp_second_val, op, variables);
      }
      else
         return handle_single_string(second_value, first_value, op, variables);
   }
   return NULL;
}

//switches string to its int or real counterpart, and changes int or real to a string
static void switch_arg_type(Value* value1, Value* value2, int type1){

   if ((value1->sval = malloc(sizeof(value1->ival))) == 0)
      Fatal("out of memory\n");
   if (type1 == INTEGER){
      sprintf(value1->sval, "%d", value1->ival);
      value2->ival = atoi(value2->sval);
      value2->type = INTEGER;
   }
   else {
      sprintf(value1->sval, "%f", value1->rval);
      value2->rval = atof(value2->sval);
      value2->type = REAL;
   }
   value1->type = STRING;
}

//concatenate two strings then put back into stack
static Value* handle_double_strings(Value* first_value, Value* second_value){
   Value* temp = newValueS("");
   char *c = malloc(sizeof(char) * (strlen(first_value->sval) + strlen(second_value->sval) + 1));
   if (c == 0) Fatal("out of memory\n");
   sprintf(c, "%s%s", second_value->sval, first_value->sval);

   temp->sval = c;
   temp->vtype = STRING;
   return temp;
}

//stores a given value into a given BST
static void store_variable(Value* token, BST* variables){
   
   if (variables->number_of_nodes == 0)
      variables->top = insert(variables->top, token);
   else
      insert(variables->top, token);

   variables->number_of_nodes++;
}




