//written by nathan brinda
#ifndef BST_H
#define BST_H
#include "BSTNode.h"

typedef struct BST{

   int number_of_nodes;
   BSTNode* top;

}BST;

BSTNode* search(BSTNode* top_of_bst, Value* value_to_delete);
BST* initializeBST();
BSTNode* insert(BSTNode* top_of_bst, Value* value);
Value* delete(BSTNode* top_of_bst, Value* value_to_delete);


//delete
//bubble down, which will be called from delete after you switch two nodes

#endif