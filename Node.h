//written by nathan brinda
#ifndef NODE_H
#define NODE_H
#include "Value.h"

//might be a better idea to have 3 of these classes
//because my_stack has access to a prev value that it does not use
//however, queue does use the prev value.
typedef struct Node{

   Value* node_value;
   struct Node* next;
   struct Node* prev;

}Node;

Node* new_node();
#endif