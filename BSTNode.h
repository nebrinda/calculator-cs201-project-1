//written by nathan brinda
#ifndef BSTNODE_H
#define BSTNODE_H
#include "Value.h"

typedef struct BSTNode{

   struct BSTNode* parent;
   struct BSTNode* left;
   struct BSTNode* right;
   Value* node_value;


}BSTNode;

BSTNode* new_BSTnode();

#endif