//written by nathan brinda
#include "BSTNode.h"
#include <stdlib.h>

BSTNode* new_BSTnode(){
   BSTNode* node = malloc(sizeof(BSTNode));
   if ((node = malloc(sizeof(BSTNode))) == 0)
      Fatal("out of memory\n");
   node->node_value = 0;
   node->left = NULL;
   node->right = NULL;
   node->parent = NULL;
   return node;
}