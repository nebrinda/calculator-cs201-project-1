//written by nathan brinda
#include "BST.h"
#include "Value.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


//protoypes for private functions
static BSTNode* find_largest_value_in_left_subtree(BSTNode* node);

static int counter = 0;

BST* initializeBST(){
   BST* my_bst = malloc(sizeof(BST));
   if ((my_bst = malloc(sizeof(BST))) == 0)
      Fatal("out of memory\n");
   my_bst->top = NULL;
   my_bst->number_of_nodes = 0;
   return my_bst;

}

BSTNode* insert(BSTNode* top_of_bst, Value* value){
   
   //no items in bst, add it to the top
   if (top_of_bst == NULL){
      top_of_bst = new_BSTnode();
      top_of_bst->node_value = value;
      return top_of_bst;
   }
   
   //if the value is smaller check the left child
   if (strcmp(top_of_bst->node_value->varname, value->varname) > 0){
      if (top_of_bst->left == NULL){
         BSTNode* node = new_BSTnode();
         node->node_value = value;
         top_of_bst->left = node; 
         top_of_bst->left->parent = top_of_bst;
         return top_of_bst;
      }
      //if the left child exists, recurse left
      else
         insert(top_of_bst->left, value);
   }
   //if the value is bigger check the right child
   else if (strcmp(top_of_bst->node_value->varname, value->varname) < 0){
       if (top_of_bst->right == NULL){
          BSTNode* node = new_BSTnode();
          node->node_value = value;
          top_of_bst->right = node;
          top_of_bst->right->parent = top_of_bst;
          return top_of_bst;
       }
       //if the child exists, recurse right
       else
          insert(top_of_bst->right, value);
    }

    return NULL;
}

Value* delete(BSTNode* top_of_bst, Value* value_to_delete){

   //search to make sure it has the item in the bst
   BSTNode* is_in = search(top_of_bst, value_to_delete);
   if (is_in == NULL)
      return NULL;

   //case1: no children
   else if (is_in->left == NULL && is_in->right == NULL){
      if (is_in->parent != NULL){
         //if is_in is the right child set his parent's left child to null
         if (is_in->parent->left == is_in)
            is_in->parent->left = NULL;
         else
            //if is_in is the right child set his parent's right child to null
            is_in->parent->right = NULL;
      }
      //int value = is_in->node_value;
      //free(is_in);
      return is_in->node_value;;
   }


   //case2: 1 child
   else if (is_in->right == NULL || is_in->left == NULL){
      //is_in has a single left ptr
      if (is_in->right == NULL){
         //parent pointer is not NULL
         if (is_in->parent != NULL){
            //value that is being deleted is the left child
            if (is_in == is_in->parent->left)
               is_in->parent->left = is_in->left;
            //value that is being deleted is the right child
            else
               is_in->parent->right = is_in->left;
         }
         is_in->left->parent = is_in->parent;
      }
      //has a single right ptr
      else{
         //parent pointer is not NULL
         if (is_in->parent != NULL){
            //value that is being deleted is the left child
            if (is_in == is_in->parent->left)
               is_in->parent->left = is_in->right;
            //value that is being deleted is the right child
            else
               is_in->parent->right = is_in->right;
         }
         is_in->right->parent = is_in->parent;
      }
      //int value = is_in->node_value;
      //free(is_in);
      return is_in->node_value;;
   }


   //case3: 2 children
   else{
      BSTNode* largest_value;
      largest_value = find_largest_value_in_left_subtree(is_in);
      is_in->node_value = largest_value->node_value;
      return delete(largest_value, largest_value->node_value);
   }
}



BSTNode* search(BSTNode* top_of_bst, Value* value_to_find){

   //not in the bst
   if (top_of_bst == NULL)
      return NULL;

   //value_to_delete is smaller then top_of_bst
   else if (strcmp(top_of_bst->node_value->varname, value_to_find->varname) > 0){
      return search(top_of_bst->left, value_to_find);
   }

   //value_to_delete is bigger then top_of_bst
   else if (strcmp(top_of_bst->node_value->varname, value_to_find->varname) < 0){
      return search(top_of_bst->right, value_to_find);
   }

   //value is equal to node
   else
      return top_of_bst;
}

//precessor
static BSTNode* find_largest_value_in_left_subtree(BSTNode* node){
   
   //only gets here if the node has 2 children, so recurse left needs no check
   if (counter == 0){
      counter++;//recurse left once
      return find_largest_value_in_left_subtree(node->left);
   }

   if (node->right == NULL)
      return node;
   else
      return find_largest_value_in_left_subtree(node->right);

}


