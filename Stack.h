//written by nathan brinda
#ifndef STACK_H
#define STACK_H
#include "Node.h"


typedef struct Stack{
   int number_of_elements;
   Node* top;
}Stack;

Value* pop(Stack* my_stack);
void push(Stack* my_stack, Value* value_to_add);
Stack* initializeStack();

#endif
