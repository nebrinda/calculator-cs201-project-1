//written by nathan brinda
#ifndef UTILS_H
#define UTILS_H

#include "Queue.h"
#include "BST.h"
#include "Value.h"

#include <stdio.h>
#include <string.h>

void print_calculated_value(Value* calculated_value);
void print_queue(Queue* queue);
void print_name();

Value* process_postfix_expression(Queue* values_to_process, BST* variables);
Queue* process_values(FILE* fp, BST* variables, char* wantPostfix);

#endif