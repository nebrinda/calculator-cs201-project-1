//modified by nathan brinda
#include "Value.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>




static Value *newValue(int);
static int determine_precedence(char op);

/**** Public Interface ****/

int NOTSET = -1;
int INTEGER = 0;
int REAL = 1;
int STRING = 2;
int VARIABLE = 3;
int OPERATOR = 4;

Value *newValueI(int i)
{
   Value *v = newValue(INTEGER);
   v->ival = i;
   return v;
}

Value *newValueR(double i)
{
   Value *v = newValue(REAL);
   v->rval = i;
   return v;
}

Value *newValueS(char* i)
{
   Value *v = newValue(STRING);
   v->sval = i;
   return v;
}

Value *newValueV(char* i)
{
   Value *v = newValue(VARIABLE);
   v->varname = i;
   return v;
}

Value *newValueO(char* i)
{
   Value *v = newValue(OPERATOR);
   v->sval = i;
   v->precedence = determine_precedence(i[0]);
   return v;
}

/**** Private Interface ****/

static Value *newValue(int t)
{
   Value *v;
   if ((v = malloc(sizeof(Value))) == 0)
     Fatal("out of memory\n");
   v->type = t;
   v->ival = 0;
   v->rval = 0;
   v->sval = 0;
   v->precedence = 0;
   v->vtype = -1; 
   v->varname = NULL;
   return v;
}

static int determine_precedence(char op){
   if (op == '+')
      return 1;
   else if (op == '-')
      return 2;
   else if (op == '*')
      return 3;
   else if (op == '/')
      return 4;
   else if (op == '%')
      return 5;
   else if (op == '^')
      return 6;
   else
      return 0;
}


void Fatal(char *fmt, ...)
{
   va_list ap;

   fprintf(stderr, "An error occured: ");
   va_start(ap, fmt);
   vfprintf(stderr, fmt, ap);
   va_end(ap);

   exit(-1);
}
